function encrypt(textType, plainText, key) {
// Check if the textbox is empty and if it only inculdes ascii characters using a regular expression
    console.log(textType);
    if (plainText.length > 0 && key.length > 0) {
        encryptHex(plainText.toUpperCase(), key);
    }
    else {
        alert("Please provide a message and a key");
    }
}

function decrypt(textType, plainText, key) {
// Check if the textbox is empty and if it only inculdes ascii characters using a regular expression
    console.log(textType);
    if (plainText.length > 0 && key.length > 0) {
        decryptHex(plainText.toUpperCase(), key);
    }
    else {
        alert("Please provide a message and a key");
    }
}

function decryptHex(plainText, key)
{
    if (isHex(plainText) && isHex(key)) {

        var bitTextArray = [];
        var initalMessagePermutation = [];
        var l = [];
        var r = [];
        var rl = [];
        var c = [];
        var cipherBinary = [];
        var cipherDecimal = [];
        var cipherText = [];

        var generatedKeys = [];
        // using a 0000 template to show the numbers as 4 bit pieces

        var leftHalfPlainText = [];
        var rightHalfPlainText = [];
        var pc1 = [57, 49, 41, 33, 25, 17, 9, 1, 58, 50, 42, 34, 26, 18, 10, 2, 59, 51, 43, 35, 27, 19, 11, 3, 60, 52, 44, 36, 63, 55, 47, 39, 31, 23, 15, 7, 62, 54, 46, 38, 30, 22, 14, 6, 61, 53, 45, 37, 29, 21, 13, 5, 28, 20, 12, 4];
        var pc2 = [14, 17, 11, 24, 1, 5, 3, 28, 15, 6, 21, 10, 23, 19, 12, 4, 26, 8, 16, 7, 27, 20, 13, 2, 41, 52, 31, 37, 47, 55, 30, 40, 51, 45, 33, 48, 44, 49, 39, 56, 34, 53, 46, 42, 50, 36, 29, 32];
        var leftShiftArray = [1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1];
        var ip = [58, 50, 42, 34, 26, 18, 10, 2, 60, 52, 44, 36, 28, 20, 12, 4, 62, 54, 46, 38, 30, 22, 14, 6, 64, 56, 48, 40, 32, 24, 16, 8, 57, 49, 41, 33, 25, 17, 9, 1, 59, 51, 43, 35, 27, 19, 11, 3, 61, 53, 45, 37, 29, 21, 13, 5, 63, 55, 47, 39, 31, 23, 15, 7];
        var e = [32, 1, 2, 3, 4, 5, 4, 5, 6, 7, 8, 9, 8, 9, 10, 11, 12, 13, 12, 13, 14, 15, 16, 17, 16, 17, 18, 19, 20, 21, 20, 21, 22, 23, 24, 25, 24, 25, 26, 27, 28, 29, 28, 29, 30, 31, 32, 1];

        //bidimensional array for 8 sBoxes
        var sBox = [
            [14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7, 0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11, 9, 5, 3, 8, 4, 1, 14, 8, 13, 6, 2, 11, 15, 12, 9, 7, 3, 10, 5, 0, 15, 12, 8, 2, 4, 9, 1, 7, 5, 11, 3, 14, 10, 0, 6, 13],
            [15, 1, 8, 14, 6, 11, 3, 4, 9, 7, 2, 13, 12, 0, 5, 10, 3, 13, 4, 7, 15, 2, 8, 14, 12, 0, 1, 10, 6, 9, 11, 5, 0, 14, 7, 11, 10, 4, 13, 1, 5, 8, 12, 6, 9, 3, 2, 15, 13, 8, 10, 1, 3, 15, 4, 2, 11, 6, 7, 12, 0, 5, 14, 9],
            [10, 0, 9, 14, 6, 3, 15, 5, 1, 13, 12, 7, 11, 4, 2, 8, 13, 7, 0, 9, 3, 4, 6, 10, 2, 8, 5, 14, 12, 11, 15, 1, 13, 6, 4, 9, 8, 15, 3, 0, 11, 1, 2, 12, 5, 10, 14, 7, 1, 10, 13, 0, 6, 9, 8, 7, 4, 15, 14, 3, 11, 5, 2, 12],
            [7, 13, 14, 3, 0, 6, 9, 10, 1, 2, 8, 5, 11, 12, 4, 15, 13, 8, 11, 5, 6, 15, 0, 3, 4, 7, 2, 12, 1, 10, 14, 9, 10, 6, 9, 0, 12, 11, 7, 13, 15, 1, 3, 14, 5, 2, 8, 4, 3, 15, 0, 6, 10, 1, 13, 8, 9, 4, 5, 11, 12, 7, 2, 14],
            [2, 12, 4, 1, 7, 10, 11, 6, 8, 5, 3, 15, 13, 0, 14, 9, 14, 11, 2, 12, 4, 7, 13, 1, 5, 0, 15, 10, 3, 9, 8, 6, 4, 2, 1, 11, 10, 13, 7, 8, 15, 9, 12, 5, 6, 3, 0, 14, 11, 8, 12, 7, 1, 14, 2, 13, 6, 15, 0, 9, 10, 4, 5, 3],
            [12, 1, 10, 15, 9, 2, 6, 8, 0, 13, 3, 4, 14, 7, 5, 11, 10, 15, 4, 2, 7, 12, 9, 5, 6, 1, 13, 14, 0, 11, 3, 8, 9, 14, 15, 5, 2, 8, 12, 3, 7, 0, 4, 10, 1, 13, 11, 6, 4, 3, 2, 12, 9, 5, 15, 10, 11, 14, 1, 7, 6, 0, 8, 13],
            [4, 11, 2, 14, 15, 0, 8, 13, 3, 12, 9, 7, 5, 10, 6, 1, 13, 0, 11, 7, 4, 9, 1, 10, 14, 3, 5, 12, 2, 15, 8, 6, 1, 4, 11, 13, 12, 3, 7, 14, 10, 15, 6, 8, 0, 5, 9, 2, 6, 11, 13, 8, 1, 4, 10, 7, 9, 5, 0, 15, 14, 2, 3, 12],
            [13, 2, 8, 4, 6, 15, 11, 1, 10, 9, 3, 14, 5, 0, 12, 7, 1, 15, 13, 8, 10, 3, 7, 4, 12, 5, 6, 11, 0, 14, 9, 2, 7, 11, 4, 1, 9, 12, 14, 2, 0, 6, 10, 13, 15, 3, 5, 8, 2, 1, 14, 7, 4, 10, 8, 13, 15, 12, 9, 0, 3, 5, 6, 11]
        ];

        var p = [16, 7, 20, 21, 29, 12, 28, 17, 1, 15, 23, 26, 5, 18, 31, 10, 2, 8, 24, 14, 32, 27, 3, 9, 19, 13, 30, 6, 22, 11, 4, 25];
        var finalP = [40, 8, 48, 16, 56, 24, 64, 32, 39, 7, 47, 15, 55, 23, 63, 31, 38, 6, 46, 14, 54, 22, 62, 30, 37, 5, 45, 13, 53, 21, 61, 29, 36, 4, 44, 12, 52, 20, 60, 28, 35, 3, 43, 11, 51, 19, 59, 27, 34, 2, 42, 10, 50, 18, 58, 26, 33, 1, 41, 9, 49, 17, 57, 25];
        // convert the message into bits

        bitTextArray = convertIntoBits(plainText, "PLAIN TEXT");
        // get the l and r parts of the message
        leftHalfPlainText = getHalf(bitTextArray, "left");
        rightHalfPlainText = getHalf(bitTextArray, "right");
        generateDiv("leftInital", "Left: " + leftHalfPlainText.join(""));
        generateDiv("rightInital", "Right: " + rightHalfPlainText.join(""));
        //generate the keys
        generatedKeys = generateKeys(key, pc1, pc2, leftShiftArray);

        //Throw away the first key
        //generatedKeys.shift();

        //Initial Message Permutation

        initalMessagePermutation = permute(bitTextArray, ip);
        generateDiv("m", "M: " + bitTextArray.join(""));
        generateDiv("initalPermutation", "IP: " + initalMessagePermutation.join(""));

        //Divide in halves for R0 and L0
        l.push(getHalf(initalMessagePermutation, "left"));
        r.push(getHalf(initalMessagePermutation, "right"));
        generateDiv("l" + 0, "L" + 0 + " : " + l[0].join(""));
        generateDiv("r" + 0, "R" + 0 + " : " + r[0].join(""));
        generateDiv("space", "</br>");

        // i = 1 because k0 is not a key and the array is 17
        for (var i = 1; i < generatedKeys.length; i++) {

            var feistelValue = [];
            l.push(r[i - 1]);
            //generate key apply keys backward
            feistelValue = feistelFunction(r[i - 1], generatedKeys[generatedKeys.length - i], sBox, e, p, i);
            r.push(xOr(l[i - 1], feistelValue));

            rl.push(r[i].join("") + l[i].join(""));

            generateDiv("l" + i, "L" + i + " : " + l[i].join(""));
            generateDiv("r" + i, "R" + i + " : " + r[i].join(""));
            generateDiv("r" + i + "l" + i, "L" + i + "R" + i + " : " + rl[i - 1]);
            generateDiv("space", "</br>");

        }

        //all invered RnLn are in the rl array final permutaion to get the cipher
        for (var i = 0; i < rl.length; i++) {
            c.push(permute(rl[i], finalP))
        }

        //last element of the list
        cipherBinary = divide(c[15], 4);
        generateDiv("space", "</br>");
        generateDiv("cipher", "Cipher bits " + c[15].join(""));

        for (var i = 0; i < cipherBinary.length; i++) {
            console.log(cipherBinary[i].join(""));
            cipherDecimal.push(parseInt(cipherBinary[i].join(""), 2));
        }
        generateDiv("cipher", "Cipher decimal " + cipherDecimal);

        for (var i = 0; i < cipherBinary.length; i++) {

            cipherText.push(cipherDecimal[i].toString(16).toUpperCase());
        }

        generateDiv("space", "</br>");
        generateDiv("cipher", "CIPHER TEXT " + cipherText.join(""));
        generateDiv("space", "</br>");

    } else
    {
        alert("You entenered " + plainText.length + " characters. The Message needs 16 characters or a non hexadecimal character was found in the string! :( ");
    }

}

function encryptHex(plainText, key) {
//length validation is also done in the regular expression
    if (isHex(plainText) && isHex(key)) {

        var bitTextArray = [];
        var initalMessagePermutation = [];
        var l = [];
        var r = [];
        var rl = [];
        var c = [];
        var cipherBinary = [];
        var cipherDecimal = [];
        var cipherText = [];

        var generatedKeys = [];
        // using a 0000 template to show the numbers as 4 bit pieces

        var leftHalfPlainText = [];
        var rightHalfPlainText = [];
        var pc1 = [57, 49, 41, 33, 25, 17, 9, 1, 58, 50, 42, 34, 26, 18, 10, 2, 59, 51, 43, 35, 27, 19, 11, 3, 60, 52, 44, 36, 63, 55, 47, 39, 31, 23, 15, 7, 62, 54, 46, 38, 30, 22, 14, 6, 61, 53, 45, 37, 29, 21, 13, 5, 28, 20, 12, 4];
        var pc2 = [14, 17, 11, 24, 1, 5, 3, 28, 15, 6, 21, 10, 23, 19, 12, 4, 26, 8, 16, 7, 27, 20, 13, 2, 41, 52, 31, 37, 47, 55, 30, 40, 51, 45, 33, 48, 44, 49, 39, 56, 34, 53, 46, 42, 50, 36, 29, 32];
        var leftShiftArray = [1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1];
        var ip = [58, 50, 42, 34, 26, 18, 10, 2, 60, 52, 44, 36, 28, 20, 12, 4, 62, 54, 46, 38, 30, 22, 14, 6, 64, 56, 48, 40, 32, 24, 16, 8, 57, 49, 41, 33, 25, 17, 9, 1, 59, 51, 43, 35, 27, 19, 11, 3, 61, 53, 45, 37, 29, 21, 13, 5, 63, 55, 47, 39, 31, 23, 15, 7];
        var e = [32, 1, 2, 3, 4, 5, 4, 5, 6, 7, 8, 9, 8, 9, 10, 11, 12, 13, 12, 13, 14, 15, 16, 17, 16, 17, 18, 19, 20, 21, 20, 21, 22, 23, 24, 25, 24, 25, 26, 27, 28, 29, 28, 29, 30, 31, 32, 1];

        //bidimensional array for 8 sBoxes
        var sBox = [
            [14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7, 0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11, 9, 5, 3, 8, 4, 1, 14, 8, 13, 6, 2, 11, 15, 12, 9, 7, 3, 10, 5, 0, 15, 12, 8, 2, 4, 9, 1, 7, 5, 11, 3, 14, 10, 0, 6, 13],
            [15, 1, 8, 14, 6, 11, 3, 4, 9, 7, 2, 13, 12, 0, 5, 10, 3, 13, 4, 7, 15, 2, 8, 14, 12, 0, 1, 10, 6, 9, 11, 5, 0, 14, 7, 11, 10, 4, 13, 1, 5, 8, 12, 6, 9, 3, 2, 15, 13, 8, 10, 1, 3, 15, 4, 2, 11, 6, 7, 12, 0, 5, 14, 9],
            [10, 0, 9, 14, 6, 3, 15, 5, 1, 13, 12, 7, 11, 4, 2, 8, 13, 7, 0, 9, 3, 4, 6, 10, 2, 8, 5, 14, 12, 11, 15, 1, 13, 6, 4, 9, 8, 15, 3, 0, 11, 1, 2, 12, 5, 10, 14, 7, 1, 10, 13, 0, 6, 9, 8, 7, 4, 15, 14, 3, 11, 5, 2, 12],
            [7, 13, 14, 3, 0, 6, 9, 10, 1, 2, 8, 5, 11, 12, 4, 15, 13, 8, 11, 5, 6, 15, 0, 3, 4, 7, 2, 12, 1, 10, 14, 9, 10, 6, 9, 0, 12, 11, 7, 13, 15, 1, 3, 14, 5, 2, 8, 4, 3, 15, 0, 6, 10, 1, 13, 8, 9, 4, 5, 11, 12, 7, 2, 14],
            [2, 12, 4, 1, 7, 10, 11, 6, 8, 5, 3, 15, 13, 0, 14, 9, 14, 11, 2, 12, 4, 7, 13, 1, 5, 0, 15, 10, 3, 9, 8, 6, 4, 2, 1, 11, 10, 13, 7, 8, 15, 9, 12, 5, 6, 3, 0, 14, 11, 8, 12, 7, 1, 14, 2, 13, 6, 15, 0, 9, 10, 4, 5, 3],
            [12, 1, 10, 15, 9, 2, 6, 8, 0, 13, 3, 4, 14, 7, 5, 11, 10, 15, 4, 2, 7, 12, 9, 5, 6, 1, 13, 14, 0, 11, 3, 8, 9, 14, 15, 5, 2, 8, 12, 3, 7, 0, 4, 10, 1, 13, 11, 6, 4, 3, 2, 12, 9, 5, 15, 10, 11, 14, 1, 7, 6, 0, 8, 13],
            [4, 11, 2, 14, 15, 0, 8, 13, 3, 12, 9, 7, 5, 10, 6, 1, 13, 0, 11, 7, 4, 9, 1, 10, 14, 3, 5, 12, 2, 15, 8, 6, 1, 4, 11, 13, 12, 3, 7, 14, 10, 15, 6, 8, 0, 5, 9, 2, 6, 11, 13, 8, 1, 4, 10, 7, 9, 5, 0, 15, 14, 2, 3, 12],
            [13, 2, 8, 4, 6, 15, 11, 1, 10, 9, 3, 14, 5, 0, 12, 7, 1, 15, 13, 8, 10, 3, 7, 4, 12, 5, 6, 11, 0, 14, 9, 2, 7, 11, 4, 1, 9, 12, 14, 2, 0, 6, 10, 13, 15, 3, 5, 8, 2, 1, 14, 7, 4, 10, 8, 13, 15, 12, 9, 0, 3, 5, 6, 11]
        ];

        var p = [16, 7, 20, 21, 29, 12, 28, 17, 1, 15, 23, 26, 5, 18, 31, 10, 2, 8, 24, 14, 32, 27, 3, 9, 19, 13, 30, 6, 22, 11, 4, 25];
        var finalP = [40, 8, 48, 16, 56, 24, 64, 32, 39, 7, 47, 15, 55, 23, 63, 31, 38, 6, 46, 14, 54, 22, 62, 30, 37, 5, 45, 13, 53, 21, 61, 29, 36, 4, 44, 12, 52, 20, 60, 28, 35, 3, 43, 11, 51, 19, 59, 27, 34, 2, 42, 10, 50, 18, 58, 26, 33, 1, 41, 9, 49, 17, 57, 25];
        // convert the message into bits

        bitTextArray = convertIntoBits(plainText, "PLAIN TEXT");
        // get the l and r parts of the message
        leftHalfPlainText = getHalf(bitTextArray, "left");
        rightHalfPlainText = getHalf(bitTextArray, "right");
        generateDiv("leftInital", "Left: " + leftHalfPlainText.join(""));
        generateDiv("rightInital", "Right: " + rightHalfPlainText.join(""));
        //generate the keys
        generatedKeys = generateKeys(key, pc1, pc2, leftShiftArray);

        //Throw away the first key
        //generatedKeys.shift();

        //Initial Message Permutation

        initalMessagePermutation = permute(bitTextArray, ip);
        generateDiv("m", "M: " + bitTextArray.join(""));
        generateDiv("initalPermutation", "IP: " + initalMessagePermutation.join(""));

        //Divide in halves for R0 and L0
        l.push(getHalf(initalMessagePermutation, "left"));
        r.push(getHalf(initalMessagePermutation, "right"));
        generateDiv("l" + 0, "L" + 0 + " : " + l[0].join(""));
        generateDiv("r" + 0, "R" + 0 + " : " + r[0].join(""));
        generateDiv("space", "</br>");

        // i = 1 because k0 is not a key and the array is 17
        for (var i = 1; i < generatedKeys.length; i++) {

            var feistelValue = [];
            l.push(r[i - 1]);
            feistelValue = feistelFunction(r[i - 1], generatedKeys[i], sBox, e, p, i);
            r.push(xOr(l[i - 1], feistelValue));

            rl.push(r[i].join("") + l[i].join(""));

            generateDiv("l" + i, "L" + i + " : " + l[i].join(""));
            generateDiv("r" + i, "R" + i + " : " + r[i].join(""));
            generateDiv("r" + i + "l" + i, "L" + i + "R" + i + " : " + rl[i - 1]);
            generateDiv("space", "</br>");

        }

        //all invered RnLn are in the rl array final permutaion to get the cipher
        for (var i = 0; i < rl.length; i++) {
            c.push(permute(rl[i], finalP))
        }

        //last element of the list
        cipherBinary = divide(c[15], 4);
        generateDiv("space", "</br>");
        generateDiv("cipher", "Cipher bits " + c[15].join(""));

        for (var i = 0; i < cipherBinary.length; i++) {
            console.log(cipherBinary[i].join(""));
            cipherDecimal.push(parseInt(cipherBinary[i].join(""), 2));
        }
        generateDiv("cipher", "Cipher decimal " + cipherDecimal);

        for (var i = 0; i < cipherBinary.length; i++) {

            cipherText.push(cipherDecimal[i].toString(16).toUpperCase());
        }

        generateDiv("space", "</br>");
        generateDiv("cipher", "CIPHER TEXT " + cipherText.join(""));
        generateDiv("space", "</br>");

    } else
    {
        alert("You entenered " + plainText.length + " characters. The Message needs 16 characters or a non hexadecimal character was found in the string! :( ");
    }
}

// r-1, key, expansion e, permutation p , index
function feistelFunction(r, key, sBox, e, p, i) {
    var expandedMsg = [];
    var xorMsg = [];
    var xorMsgChunks = [];
    var sboxProcesedMsg = [];
    var pPermutation = [];
    //console.log("R: " + r);

    //expand the 32 bit to 48 with the e permutation table
    expandedMsg = permute(r, e);
    //console.log("ER: " + expandedMsg);

    //i-1 porque este arreglo comienza desde 0 y el i esta en un valor
    //mayor por el extra key del inicio que no se usa
    generateDiv("K" + i, "K" + i + ": " + key.join(""));
    generateDiv("ER" + i - 1, "E(R" + (i - 1) + "): " + expandedMsg.join(""));

    xorMsg = xOr(expandedMsg, key);

    generateDiv("K" + i + "xorER" + i - 1, "K" + i + "xorE(R" + (i - 1) + "): " + xorMsg.join(""));
    xorMsgChunks = divide(xorMsg, 6);

    for (var j = 0; j < xorMsgChunks.length; j++) {
        sboxProcesedMsg.push(sboxProcess(xorMsgChunks[j], sBox[j], 15));
    }
    generateDiv("sBoxOutput" + i, "Sbox(K" + i + "xorE(R" + (i - 1) + ")): " + sboxProcesedMsg.join(""));
    pPermutation = permute(sboxProcesedMsg.join("").toString(), p);
    generateDiv("pPermutation" + i, "f" + (i - 1) + ": " + pPermutation.join(""));
    return pPermutation;
}

function sboxProcess(msg, sbox, sboxRowWidth) {

    var sBoxXBin = msg.slice(1, msg.length - 1);
    var sBoxYBin = [];
    var pad = "0000";
    sBoxYBin.push(msg[0]);
    sBoxYBin.push(msg[msg.length - 1]);

    // +1 seems to not be zerobased
    var sBoxXDec = parseInt(sBoxXBin.join(""), 2);
    var sBoxYDec = parseInt(sBoxYBin.join(""), 2);

    //sbox is treated as a large array.
    //figuring a start point and endpoint i can extract the row shown in the example.
    var sBoxStartPos = (sboxRowWidth + 1) * sBoxYDec;
    var sBoxEndPos = sBoxStartPos + sboxRowWidth + 1;
    var sBoxVal = sbox.slice(sBoxStartPos, sBoxEndPos)[sBoxXDec];
    var sBoxBinVal = pad.substring(0, pad.length - sBoxVal.toString(2).length) + sBoxVal.toString(2);
    /*
     console.log(msg);
     console.log("sbox: startPos " + sBoxStartPos + " endPos " + sBoxEndPos);
     console.log("sBox position: " + sBoxXDec + " , " + sBoxYDec);
     //console.log("sbox: " + sbox);
     console.log("LINE " + sbox.slice(sBoxStartPos, sBoxEndPos));
     console.log("sBoxVal: " + sBoxVal);
     console.log("BIN " + sBoxBinVal);
     console.log(" ");
     */
    return sBoxBinVal;

}

function divide(msg, msgChunkSize) {
    var msgChunks = [];
    var msgChunk = [];
    for (var i = 1; i <= msg.length; i++) {
        if (i % msgChunkSize === 0 && i !== 0) {
            //Add the segment to the chunks array
            msgChunk.push(msg[i - 1]);
            msgChunks.push(msgChunk);
            //Reset the segment
            msgChunk = [];
        }
        else {
            msgChunk.push(msg[i - 1]);
        }
    }

    return msgChunks;
}

function xOr(msg, key) {
//msg and key have to be same size
    var xorOutput = [];
    for (var i = 0; i < msg.length; i++) {
        //careful with the === it will not return the desired values since it will compare types.
        //adding a parse int bin to compare.
        if (parseInt(msg[i], 2) === parseInt(key[i], 2)) {
            xorOutput.push(0);
        }
        else {
            xorOutput.push(1);
        }
    }

    return xorOutput;
}

function generateKeys(key, pc1, pc2, leftShiftArray) {
    var bit64KeyArray = [];
    var bit56Permutation = [];
    var bit48Permutation = [];
    var cKeys = [];
    var dKeys = [];
    var combinedCDKeys = [];
    // convert the key into bits
    bit64KeyArray = convertIntoBits(key, "KEY");
    bit56Permutation = permuteModulo(bit64KeyArray, pc1, 8);
    generateDiv("1stKeyPermutation", "First key Permutation: " + bit56Permutation.join(""));
    cKeys[0] = getHalf(bit56Permutation, "left");
    dKeys[0] = getHalf(bit56Permutation, "right");
    //left shift both sides using 16 references leftShiftArray
    for (var i = 0; i < leftShiftArray.length; i++) {
        cKeys.push(leftShift16(cKeys[i], leftShiftArray[i]));
        dKeys.push(leftShift16(dKeys[i], leftShiftArray[i]));
    }

    //merge both sides of the keys
    generateDiv2Array(cKeys, dKeys, "c", "d");
    combinedCDKeys = combineSides(cKeys, dKeys);
    generateDivArray(combinedCDKeys, "c", "d");
    //permute all 17 combinedCDKeys with pc2
    for (var i = 0; i < combinedCDKeys.length; i++) {
        bit48Permutation.push(permute(combinedCDKeys[i], pc2));
    }
    generateDiv("space", "</br>");
    //Print the keys on to the HTML

    generateDivArray(bit48Permutation, "K");
    //Remove the first element K0 now the array is 16 not 17 values
    //bit48Permutation.shift();
    //generateDivArray(bit48Permutation, "K");
    generateDiv("space", "</br>");
    return bit48Permutation;
}

function combineSides(leftSide, rightSide) {
    var combinedMessage = [];
    for (var i = 0; i < leftSide.length; i++) {
        var combinedString = [];
        for (var j = 0; j < leftSide[i].length; j++) {
            combinedString.push(leftSide[i][j]);
        }

        for (var l = 0; l < rightSide[i].length; l++) {
            combinedString.push(rightSide[i][l]);
        }

        combinedMessage.push(combinedString);
    }
    return combinedMessage;
}

function leftShift16(message, numShift) {
    var shiftedMessage = [];
    //message that does not overflow
    var j = 0;
    for (var i = 0; i < message.length - numShift; i++) {
        shiftedMessage[i] = message[i + numShift];
        j++;
    }
//message that overflows
    for (var i = 0; i < numShift; i++) {
        shiftedMessage[j] = message[i];
        j++;
    }
    return shiftedMessage;
}

function convertIntoBits(message, messageType) {

    var hexTextArray = [];
    var decimalTextArray = [];
    var byteTextArray = [];
    var bitTextArray = [];
    var pad = "0000";
    var byte = [];
    for (var i = 0; i < message.length; i++) {

// Convert char to ascii number
        hexTextArray.push(message[i]);
        // Convert hex number to decimal number
        decimalTextArray.push(parseInt(hexTextArray[i], 16));
        // this method makes sure the binary numbers have exactly the length of the pad.. adding the trailing zeros i need
        byteTextArray.push(pad.substring(0, pad.length - decimalTextArray[i].toString(2).length) + decimalTextArray[i].toString(2));
        // Split each 4 bits separately into bits
        byte = byteTextArray[i].toString(2).split("");
        // for every 4 bits we assign one by one to the bit array
        for (var j = 0; j < byte.length; j++) {
            bitTextArray.push(byte[j]);
        }
    }
    

    console.log("why does have a value here javascript allows this variabla out of the for scope i=" + i);
    generateDiv("sep" + messageType, "-*-*-*-*-*-*-*-*-*-*-*-*-*-* " + messageType + " -*-*-*-*-*-*-*-*-*-*-*-*-*-*");
    generateDiv("charCnt" + messageType, "Char Count: " + message.length);
    generateDiv("hex" + messageType, "Hex values: " + hexTextArray);
    generateDiv("decimal" + messageType, "Decimal values: " + decimalTextArray);
    generateDiv("byte" + messageType, "Byte values: " + byteTextArray);
    generateDiv("bit" + messageType, "Bit values: " + bitTextArray.join(""));
    return bitTextArray;
}

function getHalf(message, side) {
    var half = [];
    switch (side) {
        case "left":
            for (var i = 0; i < message.length / 2; i++) {
                half.push(message[i]);
            }
            break;
        case "right":
            for (var i = message.length / 2; i < message.length; i++) {
                half.push(message[i]);
            }
            break;
        default:
            alert("Wrong side of message to split. it needs to be left or right.");
    }
    return half;
}

function generateDiv2Array(array1, array2, label1, label2) {
//both arrays have to be same lenght
    if (array1.length === array2.length) {
        for (var i = 0; i < array1.length; i++) {
            generateDiv(label1 + "[" + i + "]", label1 + i + ": " + array1[i].join(""));
            generateDiv(label2 + "[" + i + "]", label2 + i + ": " + array2[i].join(""));
            generateDiv("space", "</br>");
        }
    }
}

function generateDivArray(array1, label1, label2) {
    for (var i = 0; i < array1.length; i++) {
        generateDiv(label1 + "[" + i + "]" + label2 + "[" + i + "]", label1 + i + label2 + i + ": " + array1[i].join(""));
    }
}

function generateDivArray(array1, label1) {
    for (var i = 0; i < array1.length; i++) {
        generateDiv(label1 + "[" + i + "]", label1 + i + ": " + array1[i].join(""));
    }
}

function permute(seed, pt) {
    var bitPermutation = [];
    for (var i = 0; i < pt.length; i++) {
        bitPermutation.push(seed[pt[i] - 1]);
    }
    return bitPermutation;
}

function permute(seed, pt) {
    var bitPermutation = [];
    for (var i = 0; i < pt.length; i++) {
        bitPermutation.push(seed[pt[i] - 1]);
    }
    return bitPermutation;
}

function permuteModulo(seed, pt, modulo) {
    var bit56Permutation = [];
    var j = 0;
    for (var i = 0; i < seed.length; i++) {
        if (i % modulo !== 0) {
            // the permutation table is 0 based so taking away one makes this work
            bit56Permutation.push(seed[pt[j] - 1]);
            j++;
        }
    }
    return bit56Permutation;
}

function isHex(str) {
//regular expression to check if a string contains non Hex characters
    return /^[0-9A-F]{16}$/.test(str);
}

function generateDiv(idClass, innerHtml) {
    var innerDiv = document.createElement('div');
    innerDiv.id = idClass;
    innerDiv.innerHTML = innerHtml;
    document.body.appendChild(innerDiv);
}
