//functions that would work with 8 chars

function encrypt(textType, plainText) {
    // Check if the textbox is empty and if it only inculdes ascii characters using a regular expression
    if (plainText.length > 0) {
        switch (textType) {
            case "char":
                encryptChar(plainText);
                break;
            case "hex":
                encryptHex(plainText)
                break;
            default:
                alert("Please select a input type");
        }
    } else {
        alert("Please provide a message");
    }
}


function isASCII(str) {
    //regular expression to check if a string contains non ASCII characters
    return /^[\x00-\x7F]*$/.test(str);
}


function encryptChar(plainText) {
    if (isASCII(plainText) && plainText.length == 8) {

        var asciiArray = [];
        var byteArray = [];
        var bitArray = [];

        for (i = 0; i < plainText.length; i++) {

            // Convert char to ascii number
            asciiArray[i] = plainText[i].charCodeAt(0);
            // Convert ascii number to byte
            byteArray[i] = asciiArray[i].toString(2);
            // Split each 8 bytes into individual bits
            bitArray[i] = asciiArray[i].toString(2).split("");
        }

        // Write out the values into the webpage by adding a divtag
        generateDiv("separator1", "-*-*-*-*-*-*-*-*-*-*-*-*-*-* DES Algorithm ASCII Character Input -*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        generateDiv("charCount", "Char Count: " + plainText.length);
        generateDiv("asciiArray", "Ascii values: " + asciiArray);
        generateDiv("byteArray", "Byte values: " + byteArray);
        generateDiv("bitArray", "Bit values: " + bitArray);


    } else
    {
        alert("You entenered "+plainText.length+" characters. The Message needs to be 8 characters or a non hex character was found in the string! :( ");
    }

}